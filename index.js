var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);
const fs = require("fs");
const env = require("./env");
const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: env.auth_user,
    pass: env.auth_pass,
  },
});

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/chat.html");
});

app.get("/messages", function (req, res) {
  fs.readFile("./store.json", "utf-8", function (err, data) {
    if (err) {
      res.status(500);
    }

    var arrayOfObjects = [];
    if (data) {
      let existingData = JSON.parse(data);
      if (existingData && existingData.length > 0) {
        arrayOfObjects = existingData;
      }
    }
    res.status(200).json(arrayOfObjects);
  });
});

io.on("connection", function (socket) {
  console.log("a user connected");
  socket.on("joined", function (data) {
    console.log(data);
    socket.emit("acknowledge", "Acknowledged");
  });
  socket.on("post_message", function (msg) {
    const promise1 = storeMessage(msg);
    const promise2 = broadcastMessage(socket, msg);
    const promise3 = sendEmail(msg);
    const promise4 = sendSms(msg);

    Promise.all([promise1, promise2, promise3, promise4]).then((values) => {
      console.log(values);
    });
  });
});

http.listen(8080, function () {
  console.log("listening on *:8080");
});

function storeMessage(msg) {
  return new Promise((resolve, reject) => {
    fs.readFile("./store.json", "utf-8", function (err, data) {
      if (err) throw err;

      try {
        var arrayOfObjects = [];
        if (data) {
          let existingData = JSON.parse(data);
          if (existingData && existingData.length > 0) {
            arrayOfObjects = existingData;
          }
        }
        arrayOfObjects.push(msg);
        fs.writeFile(
          "./store.json",
          JSON.stringify(arrayOfObjects),
          "utf-8",
          function (err) {
            if (err) throw err;
            console.log("Done!");
            resolve(true);
          }
        );
      } catch (error) {
        console.log(error);
      }
    });
  });
}

function broadcastMessage(socket, msg) {
  return new Promise((resolve, reject) => {
    socket.emit("response_message", msg.message);
    resolve(true);
  });
}

function sendEmail(msg) {
  return new Promise((resolve, reject) => {
    //Send email to receiver

    try {
      var mailOptions = {
        from: msg.email,
        to: env.receiver_email,
        subject: "Sending Email using Node.js",
        text: msg.message,
      };

      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log("error! mail server down");
          resolve(true);
        } else {
          console.log("Email sent: " + info.response);
          resolve(true);
        }
      });
    } catch (error) {
      resolve(true);
    }
  });
}

function sendSms(msg) {
  return new Promise((resolve, reject) => {
    //Send SMS from here
    try {
      const accountSid = env.twillio_account_sid;
      const authToken = env.twillio_auth_token;
      const client = require("twilio")(accountSid, authToken);
      client.messages
        .create({
          body: msg.message,
          from: msg.phone_number,
          to: env.receiver_number,
        })
        .then((message) => resolve(true));
    } catch (error) {
      console.log("error", error);
      resolve(true);
    }
  });
}
